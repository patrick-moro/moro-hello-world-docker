FROM node:6.14.4

COPY . /app

WORKDIR /app

RUN npm install

EXPOSE 8666

CMD ["npm", "start"]

#docker build -t moro-hello-world .
#docker run -d --name moro-hello-world -p 8666:8666 moro-hello-world
#docker stop moro-hello-world
#docker rm moro-hello-world
